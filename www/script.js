(function ($) {
    jQuery.extend(jQuery.easing, {
        easeInQuad: function (x, t, b, c, d) {
            return c * (t /= d) * t + b;
        },
        easeOutQuad: function (x, t, b, c, d) {
            return -c * (t /= d) * (t - 2) + b;
        },
        easeInOutQuad: function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t + b;
            return -c / 2 * ((--t) * (t - 2) - 1) + b;
        },
        easeInCubic: function (x, t, b, c, d) {
            return c * (t /= d) * t * t + b;
        },
        easeOutCubic: function (x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t + 1) + b;
        },
        easeInOutCubic: function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t + 2) + b;
        },
        easeInQuart: function (x, t, b, c, d) {
            return c * (t /= d) * t * t * t + b;
        },
        easeOutQuart: function (x, t, b, c, d) {
            return -c * ((t = t / d - 1) * t * t * t - 1) + b;
        },
        easeInOutQuart: function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
            return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
        },
        easeInQuint: function (x, t, b, c, d) {
            return c * (t /= d) * t * t * t * t + b;
        },
        easeOutQuint: function (x, t, b, c, d) {
            return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
        },
        easeInOutQuint: function (x, t, b, c, d) {
            if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
            return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
        },
        easeInSine: function (x, t, b, c, d) {
            return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
        },
        easeOutSine: function (x, t, b, c, d) {
            return c * Math.sin(t / d * (Math.PI / 2)) + b;
        },
        easeInOutSine: function (x, t, b, c, d) {
            return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
        },
        easeInExpo: function (x, t, b, c, d) {
            return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
        },
        easeOutExpo: function (x, t, b, c, d) {
            return (t == d) ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
        },
        easeInOutExpo: function (x, t, b, c, d) {
            if (t == 0) return b;
            if (t == d) return b + c;
            if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
            return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
        },
    });

    $('body').on('click', '.menu-burger', function () {
        $('header').toggleClass('open');
        $('body').toggleClass('open-menu');
    });

	$('body').on('click', '.menu-item a', function () {
		$('header').removeClass('open');
		$('body').removeClass('open-menu');
	});

    $.transintel = function (el, options) {
        // To avoid scope issues, use 'base' instead of 'this'
        // to reference this class from internal events and functions.
        var plugin = this,
            $body = $('body');

        plugin.$el = $(el);
        plugin.el = el;

        plugin.loaded = false;

        // Add a reverse reference to the DOM object
        plugin.$el.data("transintel", plugin);

        plugin.init = function () {

            plugin.options = $.extend({}, $.transintel.defaultOptions, options);

            // Выпадающее меню услуг
            var menuDropdownTimer,
                lastId,
                topMenu = $(".menu"),
                topMenuHeight = topMenu.outerHeight(),
                menuItems = topMenu.find("a"),
                scrollItems = menuItems.map(function () {
                    var item = $($(this).attr("href"));
                    if (item.length) {
                        return item;
                    }
                });
            var buttonBottom = $('.main'),
                clickBottom = buttonBottom.find("a");

            menuItems.click(function (e) {
                var href = $(this).attr("href"),
                    offsetTop = href === "#" ? 0 : $(href).offset().top + 1;
                $('html, body').stop().animate({scrollTop: offsetTop}, 300, 'easeInOutExpo');
                e.preventDefault();
            });

            clickBottom.click(function (e) {
                var href = $(this).attr("href"),
                    offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;
                $('html, body').stop().animate({scrollTop: offsetTop}, 300, 'easeInOutExpo');
                e.preventDefault();
            });

            $('.scrollTo').click(function (e) {
                var href = $($(this).attr('href'));

                if (href.length > 0) offsetTop = (href).offset().top - topMenuHeight + 1;
                $('html, body').stop().animate({scrollTop: offsetTop}, 300, 'easeInOutExpo');

                e.preventDefault();
            });

            $(window).scroll(function () {
                var fromTop = $(this).scrollTop() + topMenuHeight + 100;

                var cur = scrollItems.map(function () {
                    if ($(this).offset().top < fromTop) return this;
                });

                cur = cur[cur.length - 1];
                var id = cur && cur.length ? cur[0].id : "";

                if (lastId !== id) {
                    lastId = id;
                    menuItems
                        .removeClass("active")
                        .filter("[href='#" + id + "']")
                        .addClass("active");
                }
            });

            // Снимаем прелоадер
            plugin.loader();
        };

        plugin.loader = function () {
            setTimeout(function () {
                plugin.loaded ? $body.addClass('loading') : $body.removeClass('loading');
            }, 20000)
        };

        // Run initializer
        plugin.init();
    };

    $.transintel.defaultOptions = {
        test: "20px"
    };

    $.fn.transintel = function (radius, options) {
        return this.each(function () {
            (new $.transintel(this, radius, options));

            // HAVE YOUR PLUGIN DO STUFF HERE


            // END DOING STUFF

        });
    };

    $(window).on('load', function () {
        var $preloader = $('#page-preloader'),
            $spinner   = $preloader.find('.spinner');
        $spinner.fadeOut();
        $preloader.delay(350).fadeOut('slow');
    });


})(jQuery);


$('body').transintel();